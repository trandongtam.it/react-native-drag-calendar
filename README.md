```javascript
<DragCalendar
  selectionAvailable={'future'}
  initMarkedDates={[]}
  theme={{
    arrowColor: '#000',
    disabledArrowColor: '#D8D8D8',
    textDisabledColor: '#D8D8D8',
    todayTextColor: '#FFA901',
    textDayHeaderFontSize: 17,
  }}
  selectionColor={'#FEEECC'}
  onSelectionChange={(newSelection: any) => {
    console.log({newSelection});
  }}
  arrowStyle={styles.arrowStyle}
  mothStyle={styles.monthStyle}
  dayHeaderStyle={styles.dayHeaderStyle}
  dayStyle={styles.dayStyle}
  filterHeight={Normalize(34)}
  baseWidth={Normalize(34)}
  borderRadius={Normalize(17)}
/>
```

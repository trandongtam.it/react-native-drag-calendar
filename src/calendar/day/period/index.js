import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {TouchableWithoutFeedback, Text, View, Platform} from 'react-native';
import {shouldUpdate} from '../../../component-updater';

import * as defaultStyle from '../../../style';
import styleConstructor, {FILLER_HEIGHT} from './style';

class Day extends Component {
  static displayName = 'IGNORE';

  static propTypes = {
    // TODO: selected + disabled props should be removed
    state: PropTypes.oneOf(['selected', 'disabled', 'today', 'hard', '']),
    // Specify theme properties to override specific styles for calendar parts. Default = {}
    theme: PropTypes.object,
    marking: PropTypes.any,
    hardMarking: PropTypes.any,
    hardMarkingColor: PropTypes.string,
    onPress: PropTypes.func,
    onLongPress: PropTypes.func,
    date: PropTypes.object,
    markingExists: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.theme = {...defaultStyle, ...(props.theme || {})};
    this.style = styleConstructor(props.theme);

    this.markingStyle = this.getDrawingStyle(props.marking || []);
    this.hardMarkingStyle = this.getHardDrawingStyle(props.hardMarking || []);
    this.onDayPress = this.onDayPress.bind(this);
    this.onDayLongPress = this.onDayLongPress.bind(this);
  }

  onDayPress() {
    this.props.onPress(this.props.date);
  }

  onDayLongPress() {
    this.props.onLongPress(this.props.date);
  }

  shouldComponentUpdate(nextProps) {
    const newMarkingStyle = this.getDrawingStyle(nextProps.marking);
    const newHardMarkingStyle = this.getHardDrawingStyle(nextProps.hardMarking);
    if (!_.isEqual(this.markingStyle, newMarkingStyle)) {
      this.markingStyle = newMarkingStyle;
      return true;
    }
    if (!_.isEqual(this.hardMarkingStyle, newHardMarkingStyle)) {
      this.hardMarkingStyle = newHardMarkingStyle;
      return true;
    }

    return shouldUpdate(this.props, nextProps, [
      'state',
      'children',
      'onPress',
      'onLongPress',
      'currentWeek',
      'hardMarking',
      'hardMarkingColor',
    ]);
  }

  getDrawingStyle(marking) {
    const defaultStyle = {textStyle: {}};
    if (!marking) {
      return defaultStyle;
    }
    if (marking.disabled) {
      defaultStyle.textStyle.color = this.theme.textDisabledColor;
    } else if (marking.selected) {
      defaultStyle.textStyle.color = this.theme.selectedDayTextColor;
    }
    const resultStyle = [marking].reduce((prev, next) => {
      if (next.quickAction) {
        if (next.first || next.last) {
          prev.containerStyle = this.style.firstQuickAction;
          prev.textStyle = this.style.firstQuickActionText;
          if (next.endSelected && next.first && !next.last) {
            prev.rightFillerStyle = '#c1e4fe';
          } else if (next.endSelected && next.last && !next.first) {
            prev.leftFillerStyle = '#c1e4fe';
          }
        } else if (!next.endSelected) {
          prev.containerStyle = this.style.quickAction;
          prev.textStyle = this.style.quickActionText;
        } else if (next.endSelected) {
          prev.leftFillerStyle = '#c1e4fe';
          prev.rightFillerStyle = '#c1e4fe';
        }
        return prev;
      }

      const color = next.color;
      if (next.status === 'NotAvailable') {
        prev.textStyle = this.style.naText;
      }
      if (next.startingDay) {
        prev.startingDay = {
          color,
        };
      }
      if (next.endingDay) {
        prev.endingDay = {
          color,
        };
      }
      if (!next.startingDay && !next.endingDay) {
        prev.day = {
          color,
        };
      }
      if (next.textColor) {
        prev.textStyle.color = next.textColor;
      }

      return prev;
    }, defaultStyle);
    return resultStyle;
  }
  getHardDrawingStyle(hardMarking) {
    const defaultStyle = {textStyle: {}};
    if (!hardMarking) {
      return defaultStyle;
    }
    if (hardMarking.disabled) {
      defaultStyle.textStyle.color = this.theme.textDisabledColor;
    } else if (hardMarking.selected) {
      defaultStyle.textStyle.color = this.theme.selectedDayTextColor;
    }
    const resultStyle = [hardMarking].reduce((prev, next) => {
      if (next.quickAction) {
        if (next.first || next.last) {
          prev.containerStyle = this.style.firstQuickAction;
          prev.textStyle = this.style.firstQuickActionText;
          if (next.endSelected && next.first && !next.last) {
            prev.rightHardFillerStyle = '#c1e4fe';
          } else if (next.endSelected && next.last && !next.first) {
            prev.leftHardFillerStyle = '#c1e4fe';
          }
        } else if (!next.endSelected) {
          prev.containerStyle = this.style.quickAction;
          prev.textStyle = this.style.quickActionText;
        } else if (next.endSelected) {
          prev.leftHardFillerStyle = '#c1e4fe';
          prev.rightHardFillerStyle = '#c1e4fe';
        }
        return prev;
      }

      const color = next.color;
      if (next.status === 'NotAvailable') {
        prev.textStyle = this.style.naText;
      }
      if (next.startingDay) {
        prev.startingDay = {
          color,
        };
      }
      if (next.endingDay) {
        prev.endingDay = {
          color,
        };
      }
      if (!next.startingDay && !next.endingDay) {
        prev.day = {
          color,
        };
      }
      if (next.textColor) {
        prev.textStyle.color = next.textColor;
      }

      return prev;
    }, defaultStyle);
    return resultStyle;
  }

  render() {
    const containerStyle = [
      this.style.base,
      {height: this.props.filterHeight, width: this.props.baseWidth},
    ];
    const textStyle = [this.style.text];
    let leftFillerStyle = {};
    let rightFillerStyle = {};
    let fillerStyle = {};
    let fillers;

    let leftHardFillerStyle = {};
    let rightHardFillerStyle = {};
    let fillerHardStyle = {};
    let hardFillers;
    const hardBorderWidth = 1.5;
    if (this.props.state === 'disabled') {
      textStyle.push(this.style.disabledText);
    } else if (this.props.state === 'today') {
      containerStyle.push(this.style.today);
      textStyle.push(this.style.todayText);
    }

    if (this.props.marking) {
      containerStyle.push({
        borderRadius: this.props.borderRadius,
      });

      const flags = this.markingStyle;
      if (flags.textStyle) {
        textStyle.push(flags.textStyle);
      }
      if (flags.containerStyle) {
        containerStyle.push(flags.containerStyle);
      }
      if (flags.leftFillerStyle) {
        leftFillerStyle.backgroundColor = flags.leftFillerStyle;
      }
      if (flags.rightFillerStyle) {
        rightFillerStyle.backgroundColor = flags.rightFillerStyle;
      }

      if (flags.startingDay && !flags.endingDay) {
        leftFillerStyle = {
          backgroundColor: this.theme.calendarBackground,
        };
        rightFillerStyle = {
          backgroundColor: flags.startingDay.color,
        };
        containerStyle.push({
          backgroundColor: flags.startingDay.color,
        });
        if (
          this.props.hardMarking.endingDay === false &&
          this.props.hardMarking.startingDay === false
        ) {
          rightHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          };
          leftHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          };
        }
      } else if (flags.endingDay && !flags.startingDay) {
        rightFillerStyle = {
          backgroundColor: this.theme.calendarBackground,
        };
        leftFillerStyle = {
          backgroundColor: flags.endingDay.color,
        };
        containerStyle.push({
          backgroundColor: flags.endingDay.color,
        });
        if (
          this.props.hardMarking.endingDay === false &&
          this.props.hardMarking.startingDay === false
        ) {
          rightHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderColor: this.props.hardMarkingColor,
          };
          leftHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderColor: this.props.hardMarkingColor,
          };
        }
      } else if (flags.day) {
        leftFillerStyle = {
          backgroundColor: flags.day.color,
        };
        rightFillerStyle = {backgroundColor: flags.day.color};
        fillerStyle = {backgroundColor: flags.day.color};
        if (
          this.props.hardMarking.endingDay === false &&
          this.props.hardMarking.startingDay === false
        ) {
          rightHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          };
          leftHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          };
        }
      } else if (flags.endingDay && flags.startingDay) {
        rightFillerStyle = {
          backgroundColor: this.theme.calendarBackground,
        };
        leftFillerStyle = {
          backgroundColor: this.theme.calendarBackground,
        };
        containerStyle.push({
          backgroundColor: flags.endingDay.color,
        });
      }

      fillers = (
        <View
          style={[
            this.style.fillers,
            fillerStyle,
            {height: this.props.filterHeight},
          ]}>
          <View
            style={[
              this.style.leftFiller,
              leftFillerStyle,
              {height: this.props.filterHeight},
            ]}
          />
          <View
            style={[
              this.style.rightFiller,
              rightFillerStyle,
              {height: this.props.filterHeight},
            ]}
          />
        </View>
      );
    }

    if (this.props.hardMarking) {
      containerStyle.push({
        borderRadius: this.props.borderRadius,
      });

      const flags = this.hardMarkingStyle;
      const flagsMarking = this.markingStyle;

      if (flags.textStyle) {
        textStyle.push(flags.textStyle);
      }
      if (flags.containerStyle) {
        containerStyle.push(flags.containerStyle);
      }
      if (flags.leftHardFillerStyle) {
        leftHardFillerStyle.backgroundColor = flags.leftHardFillerStyle;
      }
      if (flags.rightHardFillerStyle) {
        rightHardFillerStyle.backgroundColor = flags.rightHardFillerStyle;
      }

      if (
        this.props.hardMarking.endingDay === false &&
        this.props.hardMarking.startingDay === true
      ) {
        rightHardFillerStyle = {
          borderTopWidth: hardBorderWidth,
          borderBottomWidth: hardBorderWidth,
          borderTopColor: this.props.hardMarkingColor,
          borderBottomColor: this.props.hardMarkingColor,
        };
        containerStyle.push({
          borderLeftWidth: hardBorderWidth,
          borderTopWidth: hardBorderWidth,
          borderBottomWidth: hardBorderWidth,
          borderColor: this.props.hardMarkingColor,
          borderTopColor: this.props.hardMarkingColor,
          borderBottomColor: this.props.hardMarkingColor,
          borderTopRightRadius: 0,
          borderBottomRightRadius: 0,
        });

        if (flagsMarking.endingDay && flagsMarking.startingDay) {
          containerStyle.push({
            borderTopRightRadius: this.props.borderRadius,
            borderBottomRightRadius: this.props.borderRadius,
          });
        }
      } else if (
        this.props.hardMarking.endingDay === true &&
        this.props.hardMarking.startingDay === false
      ) {
        leftHardFillerStyle = {
          borderTopWidth: hardBorderWidth,
          borderBottomWidth: hardBorderWidth,
          borderTopColor: this.props.hardMarkingColor,
          borderBottomColor: this.props.hardMarkingColor,
        };
        containerStyle.push({
          borderRightWidth: hardBorderWidth,
          borderTopWidth: hardBorderWidth,
          borderBottomWidth: hardBorderWidth,
          borderBottomLeftRadius: 0,
          borderTopLeftRadius: 0,
          borderColor: this.props.hardMarkingColor,
          borderTopColor: this.props.hardMarkingColor,
          borderBottomColor: this.props.hardMarkingColor,
        });
        if (flagsMarking.endingDay && flagsMarking.startingDay) {
          containerStyle.push({
            borderBottomLeftRadius: this.props.borderRadius,
            borderTopLeftRadius: this.props.borderRadius,
          });
        }
      } else if (flags.day) {
        if (
          this.props.hardMarking.endingDay === false &&
          this.props.hardMarking.startingDay === false
        ) {
          rightHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          };
          leftHardFillerStyle = {
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          };
          containerStyle.push({
            borderTopWidth: hardBorderWidth,
            borderBottomWidth: hardBorderWidth,
            borderTopColor: this.props.hardMarkingColor,
            borderBottomColor: this.props.hardMarkingColor,
          });
        }
      } else if (
        this.props.hardMarking.endingDay &&
        this.props.hardMarking.startingDay
      ) {
        containerStyle.push({
          borderWidth: hardBorderWidth,
          borderColor: this.props.hardMarkingColor,
          borderTopColor: this.props.hardMarkingColor,
          borderBottomColor: this.props.hardMarkingColor,
        });
      }

      hardFillers = (
        <View
          style={[
            this.style.fillers,
            fillerHardStyle,
            {height: this.props.filterHeight},
          ]}>
          <View
            style={[
              this.style.leftFiller,
              leftHardFillerStyle,
              {height: this.props.filterHeight},
            ]}
          />
          <View
            style={[
              this.style.rightFiller,
              rightHardFillerStyle,
              {height: this.props.filterHeight},
            ]}
          />
        </View>
      );
    }
    return Platform.OS === 'android' ? (
      <TouchableWithoutFeedback onPress={this.onDayPress}>
        <View style={this.style.wrapper}>
          {fillers}
          {hardFillers}
          <View>
            <Text
              style={[
                textStyle,
                containerStyle,
                {
                  paddingTop: 0,
                  textAlignVertical: 'center',
                },
              ]}>
              {String(this.props.children)}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <TouchableWithoutFeedback
        testID={this.props.testID}
        onPress={this.onDayPress}
        onLongPress={this.onDayLongPress}
        disabled={this.props.marking.disableTouchEvent}
        accessible
        accessibilityRole={
          this.props.marking.disableTouchEvent ? undefined : 'button'
        }
        accessibilityLabel={this.props.accessibilityLabel}>
        <View style={[this.style.wrapper]}>
          {fillers}
          {hardFillers}
          <View style={[containerStyle, {justifyContent: 'center'}]}>
            <Text
              allowFontScaling={false}
              style={[textStyle, {paddingTop: 0}, this.props.dayStyle]}>
              {String(this.props.children)}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default Day;

import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default class drag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dayForDrag: [],
      dayLength: 0,
      itemWidth: 0,
      itemHeight: 0,
    };
  }
  setDataDay = (days) => {
    if (!!days)
      this.setState({
        dayForDrag: days,
        dayLength: days.length,
      });
  };
  componentDidMount() {
    this.setDataDay(this.props.dayForDrag);
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setDataDay(nextProps.dayForDrag);
  }

  locationToDay = (nativeEvent) => {
    const {itemWidth, itemHeight} = this.state;
    const day = {
      x: Math.floor(nativeEvent.locationX / itemWidth),
      y: Math.floor(nativeEvent.locationY / itemHeight),
    };
    return this.state.dayForDrag[day.y * 7 + day.x];
  };

  onGrant = (event) => {
    event.persist();
    const day = this.locationToDay(event.nativeEvent);
    if (day !== undefined) this.props.onSelect(day, 'grant');
  };
  onMove = (event) => {
    event.persist();
    const day = this.locationToDay(event.nativeEvent);
    if (day !== undefined) this.props.onSelect(day, 'move');
  };
  onRelese = (event) => {
    event.persist();
    const day = this.locationToDay(event.nativeEvent);
    if (day !== undefined) this.props.onSelect(day, 'release');
  };
  onLayout(event) {
    event.persist();
    const {dayLength} = this.state;
    const itemWidth = event.nativeEvent.layout.width / 7;
    const itemHeight =
      dayLength === 7
        ? event.nativeEvent.layout.height
        : dayLength === 28
        ? event.nativeEvent.layout.height / 4
        : dayLength === 42
        ? event.nativeEvent.layout.height / 6
        : event.nativeEvent.layout.height / 5;

    this.setState({
      itemWidth,
      itemHeight,
    });
  }
  render() {
    const {style, type} = this.props;
    return (
      <View
        onLayout={(event) => this.onLayout(event)}
        onStartShouldSetResponder={() => true}
        onMoveShouldSetResponderCapture={() => true}
        onMoveShouldSetResponder={() => true}
        onMoveShouldSetResponderCapture={() => true}
        onResponderGrant={(event) => this.onGrant(event)}
        onResponderMove={(event) => this.onMove(event)}
        onResponderRelease={(event) => this.onRelese(event)}
        style={[
          ss.conatiner,
          {height: style.height, width: style.width},
          type === 'week' && {left: 20, right: 20},
        ]}
      />
    );
  }
}

const ss = StyleSheet.create({
  conatiner: {
    position: 'absolute',
    zIndex: 1,
    bottom: 0,
    marginHorizontal: 5,
  },
});

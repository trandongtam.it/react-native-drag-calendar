export {default as LocaleConfig} from 'xdate';

import moment from 'moment';
import PropTypes, {element} from 'prop-types';
import React, {Component} from 'react';
import {View} from 'react-native';
import CalendarLib from './calendar';
import DragView from './drag';

const format = 'yyyy-MM-DD';
let listDayTemp = {};
let actionRemove = false;
const oneDay = 86400000;
class DragCalendar extends Component {
  constructor(props) {
    super(props);
    this.isSetInit = false;
    this.state = {
      dayForDrag: [],
      weekOfMonth: 0,
      style: {},
      markedDates: {},
      minDay: '',
      currentMonth: moment().format('MM'),
      startDayOfMonth: moment().format('DD'),
    };
  }
  convertListDayToObj = (listDay) => {
    let objListDays = {};
    if (listDay.length > 0) {
      listDay.forEach((element) => {
        objListDays[element] = {
          day: element,
          color: this.props.selectionColor,
        };
      });
    }
    return objListDays;
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!!nextProps.initMarkedDates) {
      if (
        nextProps.initMarkedDates.length > 0 &&
        nextProps.initMarkedDates.length !== this.props.initMarkedDates.length
      ) {
        const objDays = this.mergeMarkedDay(
          this.convertListDayToObj(nextProps.initMarkedDates),
        );
        this.setState({markedDates: objDays});
        this.props.onSelectionChange(objDays);
      }
    }
  }
  onLayout = (event) => {
    this.setState({dayForDrag: event.dayForDrag});
    if (event.layout)
      this.setState({
        style: {height: event.layout.height, width: event.layout.width},
      });
    const currentMonth = moment(event.currentMonth.toString()).format('MM');

    const startDay =
      currentMonth !== moment().format('MM')
        ? moment(event.currentMonth.toString()).startOf('M').format('DD')
        : moment().format('DD');
    this.setState({currentMonth, startDayOfMonth: startDay});
  };

  checkDayContinue = (list = []) => {
    const length = list.length;
    if (length === 0) return false;
    if (length === 1) return true;
    let index = 0;
    while (index < length - 1) {
      const current = parseInt(list[index].slice(8, 10));
      const next = parseInt(list[index + 1].slice(8, 10));
      if (current + 1 !== next) return false;
      index++;
    }
    return true;
  };
  onMakeDaysContinue = (objDay = {}) => {
    let objDayReturn = {...objDay};
    const list = Object.entries(objDay).sort((a, b) => (a[0] > b[0] ? 1 : -1));
    let first = list[0][0];
    const last = list[list.length - 1][0];
    while (first !== last) {
      objDayReturn[first] = {
        color: this.props.selectionColor,
        startingDay: false,
        endingDay: false,
      };
      first = moment(first).add(oneDay).format(format);
    }
    return objDayReturn;
  };
  convertDayToObj = (day = '') => {
    let returnObj = [];
    const dayObj = moment(day).format(format);
    returnObj[dayObj] = {
      color: this.props.selectionColor,
      startingDay: false,
      endingDay: false,
    };
    return returnObj;
  };
  mergeMarkedDay = (oldDays, newDays = {}) => {
    // const length = list.length;
    let returnObj = {...oldDays, ...newDays};
    const returnList = Object.entries(returnObj);

    returnList.forEach((element, index) => {
      const day = moment(element[0]).format(format);
      const nextDay = moment(element[0]).add(oneDay).format(format);
      const prevDay = moment(element[0]).subtract(oneDay).format(format);
      const dayObj = returnObj[day];
      const prevDayObj = returnObj[prevDay];
      const nextDayObj = returnObj[nextDay];

      if (prevDayObj === undefined && nextDayObj === undefined) {
        returnObj[day] = {
          startingDay: true,
          color: this.props.selectionColor,
          endingDay: true,
        };
      }
      if (prevDayObj === undefined && nextDayObj !== undefined) {
        returnObj[day] = {
          startingDay: true,
          color: this.props.selectionColor,
          endingDay: false,
        };
      }
      if (prevDayObj !== undefined && nextDayObj === undefined) {
        returnObj[day] = {
          startingDay: false,
          color: this.props.selectionColor,
          endingDay: true,
        };
      }
      if (prevDayObj !== undefined && nextDayObj !== undefined) {
        returnObj[day] = {
          startingDay: false,
          color: this.props.selectionColor,
          endingDay: false,
        };
      }
    });
    return returnObj;
  };

  removeMarkedday = (oldDays, day) => {
    let returnObj = {...oldDays};
    delete returnObj[moment(day).format(format)];
    return returnObj;
  };
  checkInclude = (days, day, type = '') => {
    if (type === 'next') return !!days[moment(day).add(oneDay).format(format)];
    if (type === 'prev')
      return !!days[moment(day).subtract(oneDay).format(format)];
    return !!days[moment(day).format(format)];
  };
  onSelect = (day, status) => {
    if (this.props.hardMarkedDates) {
      if (this.props.hardMarkedDates[moment(day).format(format)] === undefined)
        return;
    }

    if (this.props.selectionAvailable === 'future') {
      if (!!this.state.minDay) {
        if (
          moment(day).format(format) < moment(this.state.minDay).format(format)
        )
          return;
      }
      if (!!this.props.maxDate) {
        if (
          moment(day).format(format) > moment(this.props.maxDate).format(format)
        )
          return;
      }
      if (moment(day).format(format) < moment().format(format)) return;
    }
    const {markedDates} = this.state;
    if (status === 'grant') {
      if (!this.checkInclude(markedDates, day)) {
        listDayTemp = {...listDayTemp, ...this.convertDayToObj(day)};
      } else {
        actionRemove = true;
      }
    }
    if (status === 'move') {
      if (!this.checkInclude(markedDates, day)) {
        listDayTemp = this.props.hardMarkedDates
          ? {...listDayTemp, ...this.convertDayToObj(day)}
          : this.onMakeDaysContinue({
              ...listDayTemp,
              ...this.convertDayToObj(day),
            });
      }
    }

    const mergeDays = !actionRemove
      ? this.mergeMarkedDay(markedDates, listDayTemp)
      : this.mergeMarkedDay(this.removeMarkedday(markedDates, day));
    this.setState({markedDates: mergeDays});

    if (this.props.onSelectionChange) {
      if (Object.size(this.state.markedDates) !== Object.size(mergeDays))
        this.props.onSelectionChange(mergeDays);
    }
    if (status === 'release') {
      listDayTemp = {};
      actionRemove = false;
    }
  };

  render() {
    const {dayForDrag, style, markedDates, minDay} = this.state;
    const {hardMarkedDates} = this.props;
    const minDate =
      this.props.selectionAvailable === 'future'
        ? !!minDay
          ? minDay < moment().format('YYYY-MM-DD')
            ? minDay
            : moment().format('YYYY-MM-DD')
          : moment().format('YYYY-MM-DD')
        : '';
    return (
      <View>
        <CalendarLib
          {...this.props}
          markedDates={markedDates}
          hardMarkedDates={hardMarkedDates}
          hardMarkedColor={this.props.hardSelectionColor || '#000'}
          onLayout={(event) => this.onLayout(event)}
          markingType={'period'}
          minDate={minDate}
          firstDay={1}
          headerStyle={{flex: 1}}
          hideArrows={this.props.type === 'week'}
          arrowAtWeek={this.props.type === 'week'}
        />
        <DragView
          dayForDrag={dayForDrag}
          style={style}
          type={this.props.type}
          onSelect={(days, status) => this.onSelect(days.toString(), status)}
        />
      </View>
    );
  }
}
DragCalendar.propTypes = {
  onSelectionChange: PropTypes.func,
  selectionColor: PropTypes.string,
  hardSelectionColor: PropTypes.string,
  selectionAvailable: PropTypes.string,
  theme: PropTypes.object,
};
export {DragCalendar};
Object.size = function (obj) {
  var size = 0,
    key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};
